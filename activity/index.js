console.log("Hello World");

/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:




/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	

let firstName = "Yoby";
let lastName = "Fernandez";
let age = 18;
let hobbies = ["working", "home", "work again", "go back home"];
let workAddress = {
    houseNumber: 6326,
    street: "St. John street",
    city: "Paranaque City",
    state: "NCR"
}

console.log("My name is " + firstName);
console.log("My last name is " + lastName);
console.log("My full name is " + firstName + " " + lastName);
console.log("My current age is " + age);
console.log("My hobbies are " + hobbies);
console.log("My working address: ");
console.log(workAddress);

// debug

let fullName = "Steve Rogers";
console.log("My full name is: " + fullName);

let currentAge = 40;
console.log("My current age is: " + currentAge);

let friends = ["Tony", "Bruce", "Thor", "Natasha", "Clint", "Nick"];
console.log("My Friends are: ")
console.log(friends);

let profile = {
    username: "captain_america",
    fullName: "Steve Rogers",
    age: 40,
    isActive: false,
}

console.log("My Full Profile: ")
console.log(profile);

fullName = "Bucky Barnes";
console.log("My bestfriend is: " + fullName);

const lastLocation = "Arctic Ocean";
// let lastLocation = "Arctic Ocean";
// lastLocation = "Atlantic Ocean";
console.log("I was found frozen in: " + lastLocation);